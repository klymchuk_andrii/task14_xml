package com.klymchuk.model.parser;

import com.klymchuk.model.Knife;
import jdk.internal.org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class SaxParser {
    public void parse(){
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            SaxHandler handler = new SaxHandler();
            saxParser.parse(new File("C:\\Users\\Andrii\\IdeaProjects\\task14\\src\\main\\resources\\KnifeXML.xml"), handler);
            //Get Knife list
            List<Knife> knifeList = handler.getKnifeList();
            //print knife information
            for(Knife knife : knifeList)
                System.out.println(knife);
        } catch (ParserConfigurationException | IOException | org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
    }
}
