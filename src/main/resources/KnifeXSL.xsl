<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>
                    Knifes
                </h2>
                <table border="1">
                    <tr>
                        <th>Type</th>
                        <th>Handy</th>
                        <th>Origin</th>
                        <th>Length</th>
                        <th>Width</th>
                        <th>Material</th>
                        <th>Sleeve</th>
                        <th>Blood flow</th>
                        <th>Value</th>
                    </tr>
                    <xsl:for-each select="Knifes/Knife">
                        <xsl:sort select="Length"/>
                        <tr>
                            <td>
                                <xsl:value-of select="Type"/>
                            </td>
                            <td>
                                <xsl:value-of select="Handy"/>
                            </td>
                            <td>
                                <xsl:value-of select="Origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="Visual/Blade/Length"/>
                            </td>
                            <td>
                                <xsl:value-of select="Visual/Blade/Width"/>
                            </td>
                            <td>
                                <xsl:value-of select="Visual/Material"/>
                            </td>
                            <td>
                                <xsl:value-of select="Visual/Sleeve"/>
                            </td>
                            <td>
                                <xsl:value-of select="Visual/BloodFlow"/>
                            </td>
                            <td>
                                <xsl:value-of select="Value"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>