package com.klymchuk.model.parser;

import com.klymchuk.model.Blade;
import com.klymchuk.model.Knife;
import com.klymchuk.model.KnifeType;
import com.klymchuk.model.KnifeVisual;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomParser {
    private List<Knife> knifeList = new ArrayList<>();

    public void parse() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File("C:\\Users\\Andrii\\IdeaProjects\\task14\\src\\main\\resources\\KnifeXML.xml"));

        document.getDocumentElement().normalize();

        Element root = document.getDocumentElement();
        System.out.println(root.getNodeName());

        NodeList nList = document.getElementsByTagName("Knife");
        System.out.println("============================");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                getElement(node);
            }
        }
    }

    private void getElement(Node node) {
        Element eElement = (Element) node;

        Blade blade = new Blade(
                Integer.parseInt(eElement.getElementsByTagName("Length").item(0).getTextContent()),
                Integer.parseInt(eElement.getElementsByTagName("Width").item(0).getTextContent())
        );

        KnifeVisual visual = new KnifeVisual(
                blade,
                eElement.getElementsByTagName("Material").item(0).getTextContent(),
                eElement.getElementsByTagName("Sleeve").item(0).getTextContent(),
                Boolean.parseBoolean(eElement.getElementsByTagName("BloodFlow").item(0).getTextContent())
        );

        Knife knife = new Knife(
                KnifeType.valueOf(eElement.getElementsByTagName("Type").item(0).getTextContent()),
                Integer.parseInt(eElement.getElementsByTagName("Handy").item(0).getTextContent()),
                eElement.getElementsByTagName("Origin").item(0).getTextContent(),
                visual,
                Boolean.parseBoolean(eElement.getElementsByTagName("Value").item(0).getTextContent())
        );

        knifeList.add(knife);
    }

    public List<Knife> getKnifeList() {
        return knifeList;
    }
}
