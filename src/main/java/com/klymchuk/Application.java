package com.klymchuk;

import com.klymchuk.model.parser.DomParser;
import com.klymchuk.model.parser.SaxParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Application.class);

        DomParser domParser = new DomParser();
        try {
            domParser.parse();
            logger.info(domParser.getKnifeList());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        SaxParser saxParser = new SaxParser();
        saxParser.parse();
    }
}
