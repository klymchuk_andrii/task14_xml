package com.klymchuk.model;

import java.util.Comparator;

public class Knife implements Comparator<Knife> {
    private KnifeType type;
    private int handy;
    private String origin;
    private KnifeVisual visual;
    private boolean value;

    public Knife(KnifeType type, int handy, String origin, KnifeVisual visual, boolean value) {
        this.type = type;
        this.handy = handy;
        this.origin = origin;
        this.visual = visual;
        this.value = value;
    }

    public Knife() {
        this.type = null;
        this.handy = 0;
        this.origin = null;
        this.visual = null;
        this.value = false;
    }

    public KnifeType getType() {
        return type;
    }

    public void setType(KnifeType type) {
        this.type = type;
    }

    public int getHandy() {
        return handy;
    }

    public void setHandy(int handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public KnifeVisual getVisual() {
        return visual;
    }

    public void setVisual(KnifeVisual visual) {
        this.visual = visual;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Knife{" +
                "type=" + type +
                ", handy=" + handy +
                ", origin='" + origin + '\'' +
                ", visual=" + visual +
                ", value=" + value +
                "}\n";
    }


    @Override
    public int compare(Knife o1, Knife o2) {
        return o2.getVisual().getBlade().getLength()-o1.getVisual().getBlade().getLength();
    }
}
