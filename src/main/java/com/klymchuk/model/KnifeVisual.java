package com.klymchuk.model;

public class KnifeVisual {
    private Blade blade;
    private String material;
    private String sleeve;
    private boolean bloodFlow;

    public KnifeVisual(Blade blade, String material, String sleeve, boolean bloodFlow) {
        this.blade = blade;
        this.material = material;
        this.sleeve = sleeve;
        this.bloodFlow = bloodFlow;
    }

    public KnifeVisual() {
        this.blade = null;
        this.material = null;
        this.sleeve = null;
        this.bloodFlow = false;
    }

    public Blade getBlade() {
        return blade;
    }

    public void setBlade(Blade blade) {
        this.blade = blade;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getSleeve() {
        return sleeve;
    }

    public void setSleeve(String sleeve) {
        this.sleeve = sleeve;
    }

    public boolean isBloodFlow() {
        return bloodFlow;
    }

    public void setBloodFlow(boolean bloodFlow) {
        this.bloodFlow = bloodFlow;
    }

    @Override
    public String toString() {
        return "KnifeVisual{" +
                "blade=" + blade +
                ", material='" + material + '\'' +
                ", sleeve='" + sleeve + '\'' +
                ", bloodFlow=" + bloodFlow +
                "}";
    }
}
