package com.klymchuk.model.parser;

import com.klymchuk.model.Blade;
import com.klymchuk.model.Knife;
import com.klymchuk.model.KnifeType;
import com.klymchuk.model.KnifeVisual;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {
    private List<Knife> knifeList = null;
    private Knife knife = null;
    private KnifeVisual knifeVisual = null;
    private Blade blade = null;
    private StringBuilder data = null;

    public List<Knife> getKnifeList() {
        return knifeList;
    }

    private boolean bType;
    private boolean bHandy;
    private boolean bOrigin;
    private boolean bVisual;
    private boolean bBlade;
    private boolean bLength;
    private boolean bWidth;
    private boolean bMaterial;
    private boolean bSleeve;
    private boolean bBloodFlow;
    private boolean bValue;


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if (qName.equalsIgnoreCase("Knife")) {
            knife = new Knife();
            // initialize list
            if (knifeList == null)
                knifeList = new ArrayList<>();
        } else if (qName.equalsIgnoreCase("type")) {
            // set boolean values for fields, will be used in setting Employee variables
            bType = true;
        } else if (qName.equalsIgnoreCase("handy")) {
            bHandy = true;
        } else if (qName.equalsIgnoreCase("origin")) {
            bOrigin = true;
        } else if (qName.equalsIgnoreCase("visual")) {
            knifeVisual = new KnifeVisual();
            bVisual = true;
        } else if (qName.equalsIgnoreCase("blade")) {
            blade = new Blade();
            bBlade = true;
        } else if (qName.equalsIgnoreCase("length")) {
            bLength = true;
        } else if (qName.equalsIgnoreCase("width")) {
            bWidth = true;
        } else if (qName.equalsIgnoreCase("material")) {
            bMaterial = true;
        } else if (qName.equalsIgnoreCase("sleeve")) {
            bSleeve = true;
        } else if (qName.equalsIgnoreCase("bloodFlow")) {
            bBloodFlow = true;
        } else if (qName.equalsIgnoreCase("value")) {
            bValue = true;
        }
        // create the data container
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (bType) {
            // age element, set Employee age
            knife.setType(KnifeType.valueOf(data.toString()));
            bType = false;
        } else if (bHandy) {
            knife.setHandy(Integer.parseInt(data.toString()));
            bHandy = false;
        } else if (bOrigin) {
            knife.setOrigin(data.toString());
            bOrigin = false;
        } else if (bVisual) {
            knife.setVisual(knifeVisual);
            bVisual = false;
        } else if (bBlade) {
            knifeVisual.setBlade(blade);
            bBlade = false;
        } else if (bLength) {
            blade.setLength(Integer.parseInt(data.toString()));
            bLength = false;
        } else if (bWidth) {
            blade.setWidth(Integer.parseInt(data.toString()));
            bWidth = false;
        } else if (bMaterial) {
            knifeVisual.setMaterial(data.toString());
            bMaterial = false;
        } else if (bSleeve) {
            knifeVisual.setSleeve(data.toString());
            bSleeve = false;
        } else if (bBloodFlow) {
            knifeVisual.setBloodFlow(Boolean.parseBoolean(data.toString()));
            bBloodFlow = false;
        } else if (bValue) {
            knife.setValue(Boolean.parseBoolean(data.toString()));
            bValue = false;
        }

        if (qName.equalsIgnoreCase("Knife")) {
            knifeList.add(knife);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        data.append(new String(ch, start, length));
    }
}
